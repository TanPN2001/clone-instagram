import {
  List,
  ListItem,
  ListSubheader,
  ListItemAvatar,
  ListItemButton,
  ListItemText,
  Avatar,
  Card,
  Paper,
  Divider,
  Button,
  CircularProgress,
  LinearProgress
} from "@mui/material";
// import { Box } from "@mui/system";
import { useQuery } from "@tanstack/react-query";
import { useContext } from "react";
import { Link } from "react-router-dom";
import { makeRequest } from "../../axios";
import { DarkModeContext } from "../../context/darkModeContext";
export default function Followers({ }) {
  const { darkMode } = useContext(DarkModeContext);
  const {
    isLoading,
    error,
    data: followers,
  } = useQuery(["followers"], async () => {
    const res = await makeRequest.get("user/profile", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    });
    console.log(res.data.user.followers);
    return res.data.user.followers;
  });
  return (
    <Paper
      sx={{
        backgroundColor: darkMode ? "#333333" : "#f6f3f3",
        minHeight: "100vh",
        pt: 5,
        pb: 5,
      }}
    >
      {isLoading ? (
        <LinearProgress sx={{ textAlign: "center" }} />
      ) : error ? (
        "error"
      ) : (
        <Card
          sx={{
            m: 5,
            mb: 0,
            mt: 0,
            // position: "relative",
            // top: "40px",
            minHeight: "100vh",
            borderRadius: 2,
          }}
        >
          <List
            sx={{
              width: "100%",
              bgcolor: "background.paper",
            }}
            subheader={
              <ListSubheader
                id="nested-list-subheader"
                sx={{ fontSize: "150%" }}
              >
                Người theo dõi
                <Divider />
              </ListSubheader>
            }
          >
            {followers.map((follower) => (
              <ListItemButton
                component={Link}
                to={`/profile/${follower.id}`}
                divider
              >
                <ListItemAvatar>
                  <Avatar src={follower.avatar}></Avatar>
                </ListItemAvatar>
                <ListItemText
                  primary={follower.fullname}
                  primaryTypographyProps={{ fontSize: "24px" }}
                />
                <Button ></Button>
              </ListItemButton>
            ))}
          </List>
        </Card>
      )}
    </Paper>
  );
}
