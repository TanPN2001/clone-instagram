import {
    List,
    ListItem,
    ListSubheader,
    ListItemAvatar,
    ListItemButton,
    ListItemText,
    Avatar,
    Card,
    Paper,
    Divider,
    Button,
    CircularProgress,
    LinearProgress,
    Fade,
    Slide,
    Collapse
} from "@mui/material";
// import { Box } from "@mui/system";
import { useQuery, useQueryClient } from "@tanstack/react-query";
import { useContext } from "react";
import { Link } from "react-router-dom";
import { TransitionGroup } from "react-transition-group";
import { makeRequest } from "../../axios";
import { DarkModeContext } from "../../context/darkModeContext";
export default function Following({ }) {
    const queryClient = useQueryClient()
    const { darkMode } = useContext(DarkModeContext);
    const {
        isLoading,
        error,
        data: followingArr,
    } = useQuery(["following"], async () => {
        const res = await makeRequest.get("user/profile", {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
        });
        console.log(res.data);
        return res.data.user.followings;
    });
    return (
        <Paper
            sx={{
                backgroundColor: darkMode ? "#333333" : "#f6f3f3",
                minHeight: "100vh",
                pt: 5,
                pb: 5,
            }}
        >
            {isLoading ? (
                <LinearProgress />
            ) : error ? (
                "error"
            ) : (
                <Card
                    sx={{
                        m: 5,
                        mb: 0,
                        mt: 0,
                        // position: "relative",
                        // top: "40px",
                        minHeight: "100vh",
                        borderRadius: 2,
                    }}
                >
                    <List
                        sx={{
                            width: "100%",
                            bgcolor: "background.paper",
                        }}
                        subheader={
                            <ListSubheader
                                id="nested-list-subheader"
                                sx={{ fontSize: "150%" }}
                            >
                                Đang theo dõi
                                <Divider />
                            </ListSubheader>
                        }
                    >
                        <TransitionGroup>
                            {followingArr.map((following) => (
                                <Collapse key={following.id}>
                                    <ListItem
                                        sx={{ p: 1 }}
                                        secondaryAction={(<Button variant="contained" sx={{ borderRadius: '9999px 9999px 9999px 9999px' }} onClick={() => {
                                            makeRequest.put(`/user/unfollow/${following.id}`, {}, {
                                                headers: {
                                                    Authorization: `Beare ${localStorage.getItem("token")}`
                                                }
                                            }).then((res) => {
                                                console.log(res)
                                                queryClient.invalidateQueries(["following"])
                                                queryClient.invalidateQueries(["suggestion"])
                                            })
                                        }}>Hủy theo dõi</Button>)}>
                                        <ListItemButton
                                            component={Link}
                                            to={`/profile/${following.id}`}
                                            divider
                                        >
                                            <ListItemAvatar>
                                                <Avatar src={following.avatar}></Avatar>
                                            </ListItemAvatar>
                                            <ListItemText
                                                primary={following.fullname}
                                                primaryTypographyProps={{ fontSize: "24px" }}
                                            />

                                        </ListItemButton>
                                    </ListItem>
                                </Collapse>
                            ))}</TransitionGroup>

                    </List>
                </Card>
            )}
        </Paper>
    );
}
