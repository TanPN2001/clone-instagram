import { useState, useRef, useContext } from "react";
import { Link, useNavigate } from "react-router-dom";
import "./register.scss";
import axios from "axios";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import Slide from "@mui/material/Slide";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { AuthContext } from "../../context/authContext";

const Register = () => {
  const { login, token, logout } = useContext(AuthContext);
  const [page2Dir, setPage2Dir] = useState("left");

  const [inputs, setInputs] = useState({
    email: "",
    password: "",
    fullname: "",
  });
  const [err, setErr] = useState(null);

  const handleChange = (e) => {
    setInputs((prev) => ({ ...prev, [e.target.name]: e.target.value }));
  };

  const handleClick = async (e) => {
    e.preventDefault();

    await axios
      .post("https://be-web.onrender.com/user/register", inputs)
      .then((response) => {
        console.log(response.data);
        setErr(response.data.reason);
        if (response.data.result == "success") {
          login({
            email: inputs.email,
            password: inputs.password,
          });
          setPageNumber(2);
        }
      });
  };

  const [file, setFile] = useState();
  const [pageNumber, setPageNumber] = useState(1);

  const navigate = useNavigate();
  const fileInputRef = useRef(null);

  // console.log(err);

  return (
    <div className="register">
      <Slide
        in={pageNumber === 1}
        direction={pageNumber === 1 ? "left" : "right"}
        mountOnEnter
        unmountOnExit
      >
        <div className="card">
          <div className="left">
            <h1>Instagram</h1>
            <p>
              Social networking site for young people. New experiences will
              always be around us.{" "}
            </p>
            <span>Do you have an account?</span>
            <Link to="/login">
              <button>Login</button>
            </Link>
          </div>
          <div className="right">
            <h1>Register</h1>
            <form>
              <input
                type="email"
                placeholder="Email"
                name="email"
                onChange={handleChange}
              />
              <input
                type="password"
                placeholder="Password"
                name="password"
                onChange={handleChange}
              />
              <input
                type="text"
                placeholder="Name"
                name="fullname"
                onChange={handleChange}
              />
              {err && err}
              <button onClick={handleClick}>Register</button>
            </form>
          </div>
        </div>
      </Slide>
      <Slide
        in={pageNumber === 2}
        direction={page2Dir}
        mountOnEnter
        unmountOnExit
      >
        <Card position="flex" alignItems="center" justifyContent="center">
          <CardContent>
            <Typography
              sx={{ fontSize: 14 }}
              color="text.secondary"
              gutterBottom
            >
              Cập nhật ảnh đại diện
            </Typography>
            <Typography variant="h5" component="div">
              Bạn có thể thay đổi ảnh đại diện bất cứ khi nào bạn muốn
            </Typography>
          </CardContent>
          <CardActions
            sx={{
              alignSelf: "stretch",
              display: "flex",
              justifyContent: "flex-end",
              alignItems: "flex-start",
            }}
          >
            <Button
              size="small"
              onClick={() => {
                logout();
                navigate("/");
              }}
            >
              Để sau
            </Button>
            <Button
              size="small"
              variant="contained"
              onClick={() => {
                fileInputRef.current.click();
              }}
            >
              Cập nhật
              <input
                type="file"
                accept=".png, .img"
                hidden
                onChange={(e) => {
                  setFile(e.target.files[0]);
                  setPage2Dir("right");
                  setPageNumber(3);
                }}
                ref={fileInputRef}
              ></input>
            </Button>
          </CardActions>
        </Card>
        {/* <AddAvatar /> */}
      </Slide>
      <Slide
        direction={"left"}
        in={pageNumber === 3}
        mountOnEnter
        unmountOnExit
      >
        <Card position="flex" alignItems="center" justifyContent="center">
          <CardContent>
            <Typography
              sx={{ fontSize: 14 }}
              color="text.secondary"
              gutterBottom
            >
              Bạn có chắc muốn sử dụng ảnh đại diện này không
            </Typography>
            <Typography variant="h5" component="div">
              Bạn có thể thay đổi ảnh đại diện bất cứ khi nào bạn muốn
            </Typography>
            <img
              src={file ? URL.createObjectURL(file) : ""}
              width="200px"
              height="200px"
            />
          </CardContent>
          <CardActions
            sx={{
              alignSelf: "stretch",
              display: "flex",
              justifyContent: "flex-end",
              alignItems: "flex-start",
            }}
          >
            <Button
              size="small"
              onClick={() => {
                setPageNumber(2);
              }}
            >
              Quay lại
            </Button>
            <Button
              size="smale"
              onClick={() => {
                const formData = new FormData();
                formData.append("avatar", file);
                axios
                  .put(
                    "https://be-web.onrender.com/user/upload-avatar",
                    formData,
                    {
                      headers: {
                        Authorization: `Bearer ${token}`,
                      },
                    }
                  )
                  .then((res) => {
                    logout();
                    navigate("/");
                  });
              }}
              variant="contained"
            >
              Cập nhât
            </Button>
          </CardActions>
        </Card>
      </Slide>
    </div>
  );
};

export default Register;
