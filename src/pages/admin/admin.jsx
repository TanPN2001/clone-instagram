import {
    List,
    ListItem,
    ListSubheader,
    ListItemAvatar,
    ListItemButton,
    ListItemText,
    Avatar,
    Card,
    Paper,
    Divider,
    Button,
    CircularProgress,
    LinearProgress,
    Collapse,
  } from "@mui/material";
  
  // import { Box } from "@mui/system";
  import { useQuery } from "@tanstack/react-query";
  import { useContext } from "react";
  import { useState } from "react";
  import { Link } from "react-router-dom";
  import { makeRequest } from "../../axios";
  import { DarkModeContext } from "../../context/darkModeContext";
  import ExpandLess from "@mui/icons-material/ExpandLess";
  import ExpandMore from "@mui/icons-material/ExpandMore";
  import UserList from "../../components/userList/userList";
  import Chart from "../../components/chart/chart";
import ReportList from "../../components/reportList/reportList";
  export default function Admin({}) {
    const [open, setOpen] = useState({ userList: false, chart: false, reportList: false });
    const { darkMode } = useContext(DarkModeContext);
    const {
      isLoading,
      error,
      data: followers,
    } = useQuery(["followers"], async () => {
      const res = await makeRequest.get("user/profile", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      //console.log(res.data.user.followers);
      return res.data.user.followers;
    });
    return (
      <Paper
        sx={{
          backgroundColor: darkMode ? "#333333" : "#f6f3f3",
          minHeight: "100vh",
          pt: 5,
          pb: 5,
        }}
      >
        {isLoading ? (
          <LinearProgress sx={{ textAlign: "center" }} />
        ) : error ? (
          "error"
        ) : (
          <Card
            sx={{
              m: 5,
              mb: 0,
              mt: 0,
              // position: "relative",
              // top: "40px",
              minHeight: "100vh",
              borderRadius: 2,
            }}
          >
            <List
              sx={{
                width: "100%",
                bgcolor: "background.paper",
              }}
              subheader={
                <ListSubheader
                  id="nested-list-subheader"
                  sx={{ fontSize: "150%" }}
                >
                  Trình quản lý
                  <Divider />
                </ListSubheader>
              }
            >
              <ListItemButton
                onClick={() => {
                  setOpen((state) => {
                    return { ...state, userList: !state.userList };
                  });
                }}
              >
                {" "}
                <ListItemText primary="Danh sách người dùng" />
                {open.userList ? <ExpandLess /> : <ExpandMore />}
              </ListItemButton>
              
              <Collapse in={open.userList} timeout="auto" unmountOnExit>
                <UserList
                  style={{
                    paddingLeft: "20px",
                  }}
                />
              </Collapse>

              <ListItemButton
                onClick={() => {
                  setOpen((state) => {
                    return { ...state, chart: !state.chart };
                  });
                }}
              >
                {" "}
                <ListItemText primary="Biểu đồ" />
                {open.chart ? <ExpandLess /> : <ExpandMore />}
              </ListItemButton>

              <Collapse in={open.chart} timeout="auto" unmountOnExit>
                <Chart
                  style={{
                    paddingLeft: "20px",
                  }}
                />
              </Collapse>

              <ListItemButton
                onClick={() => {
                  setOpen((state) => {
                    return { ...state, reportList: !state.reportList };
                  });
                }}
              >
                {" "}
                <ListItemText primary="Báo cáo" />
                {open.reportList ? <ExpandLess /> : <ExpandMore />}
              </ListItemButton>

              <Collapse in={open.reportList} timeout="auto" unmountOnExit>
                <ReportList
                  style={{
                    paddingLeft: "20px",
                  }}
                />
              </Collapse>
            </List>
          </Card>
        )}
      </Paper>
    );
  }
  