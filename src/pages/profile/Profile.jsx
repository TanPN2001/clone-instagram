import "./profile.scss";
import FacebookTwoToneIcon from "@mui/icons-material/FacebookTwoTone";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import InstagramIcon from "@mui/icons-material/Instagram";
import PinterestIcon from "@mui/icons-material/Pinterest";
import TwitterIcon from "@mui/icons-material/Twitter";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import PlaceIcon from "@mui/icons-material/Place";
import LanguageIcon from "@mui/icons-material/Language";
import EmailOutlinedIcon from "@mui/icons-material/EmailOutlined";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import { LinearProgress } from "@mui/material";
import Posts from "../../components/posts/Posts";
import { useQuery, useQueryClient, useMutation } from "@tanstack/react-query";
import { makeRequest } from "../../axios";
import { useLocation, useParams } from "react-router-dom";
import { useContext, useEffect } from "react";
import { AuthContext } from "../../context/authContext";
import Update from "../../components/update/Update";
import { useState } from "react";
import axios from "axios";
import Share from "../../components/share/Share";
import ReportModal from "../../components/reportModal/reportModal";
import ReportModalUser from "../../components/reportModalUser/ReportModalUser";

const Profile = () => {
  const [openUpdate, setOpenUpdate] = useState(false);
  const { currentUser } = useContext(AuthContext);
  const [openDelete, setOpenDelete] = useState(false);
  const [openReport, setOpenReport] = useState(false);

  const { id: userId } = useParams();

  const queryClient = useQueryClient();

  const {
    isLoading,
    error,
    data: user,
  } = useQuery(["user", userId], () =>
    makeRequest
      .get("/user/" + userId, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
      .then((res) => {
        return res.data.user;
      })
  );

  const mutation = useMutation(
    (userId) => {
      return makeRequest
        .put(
          `user/${
            user.followers.find((follower) => follower.id == currentUser._id)
              ? "unfollow"
              : "follow"
          }/${userId}`,
          {},
          {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          }
        )
        .then((res) => {
          // console.log(res)
        });
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries(["user"]);
        queryClient.invalidateQueries(["suggestion"]);
      },
    }
  );

  const handleFollow = () => {
    mutation.mutate(userId);
  };

  return isLoading ? (
    <LinearProgress />
  ) : (
    <div className="profile">
      <>
        <div className="images">
          <img src={user.avatar} alt="" className="avatar" />
          {/* <img
              src={user.avatar}
              alt=""
              className="profilePic"
            /> */}
        </div>
        <div className="profileContainer">
          <div className="uInfo">
            <div className="center">
              <span style={{ marginTop: 10 }}>{user.fullname}</span>

              <div className="info">
                <div className="item">
                  <PlaceIcon />
                </div>
                <div className="item">
                  <LanguageIcon />
                </div>
                <div className="item">
                  <EmailOutlinedIcon />
                </div>

                <div className="item">
                  <div className="dropdown">
                    <button
                      className="btn item"
                      type="button"
                      id="dropdownMenuButton"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                      style={{ backgroundColor: "transparent" }}
                    >
                      <MoreHorizIcon></MoreHorizIcon>
                    </button>
                    <div
                      className="dropdown-menu"
                      aria-labelledby="dropdownMenuButton"
                    >
                      <a
                        className="dropdown-item"
                        href="#"
                        onClick={() => {
                          setOpenReport(true);
                        }}
                      >
                        Báo cáo
                      </a>

                      <a
                        class="dropdown-item"
                        href="#"
                        onClick={() => {
                          setOpenDelete(true);
                        }}
                      >
                        Xóa
                      </a>

                      <a
                        class="dropdown-item"
                        href="#"
                        onClick={() => {
                          setOpenUpdate(true);
                        }}
                      >
                        Cập nhật
                      </a>
                    </div>
                  </div>
                </div>
              </div>

              {userId !== currentUser._id && (
                <button
                  style={{ margin: 10, width: "30%" }}
                  onClick={handleFollow}
                >
                  {user.followers.find(
                    (follower) => follower.id == currentUser._id
                  )
                    ? "Following"
                    : "Follow"}
                </button>
              )}
            </div>
          </div>
          {userId === currentUser._id && <Share></Share>}

          <Posts userId={userId} />
          <ReportModalUser open={openReport} setOpen={setOpenReport} user={user} type="USER"></ReportModalUser>
          {/* <ReportModal open={openReport} setOpen={setOpenReport} user={user} type="USER"></ReportModal> */}
        </div>
      </>

      {openUpdate && <Update setOpenUpdate={setOpenUpdate} user={user} />}
    </div>
  );
};

export default Profile;
