import { createContext, useEffect, useState } from "react";

export const LeftBarContext = createContext();

export const LeftBarContextProvider = ({ children }) => {
  const [leftBar, setLeftBar] = useState("feed");

  const change = (part) => {
    setLeftBar(part);
  };

  return (
    <LeftBarContext.Provider value={{ leftBar, change }}>
      {children}
    </LeftBarContext.Provider>
  );
};
