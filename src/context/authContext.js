import axios from "axios";
import { createContext, useEffect, useState } from "react";
import { makeRequest } from "../axios";

export const AuthContext = createContext();

export const AuthContextProvider = ({ children }) => {
  const [token, settoken] = useState(localStorage.getItem("token") || null);
  const [currentUser, setCurrentUser] = useState(
    JSON.parse(localStorage.getItem("user") || null) || null
  );

  const login = async (inputs) => {
    try {
      const res = await makeRequest.post('/user/login', inputs)
      // const res = await axios.post(
      //   "https://be-web.onrender.com/user/login",
      //   inputs
      // );
      // console.log(res);
      //TODO add error checking
      if (res.data.result == "success") {
        localStorage.setItem("token", res.data.accessToken);
        settoken(localStorage.getItem("token"));

        const Cuser = await axios.get(
          "https://be-web.onrender.com/user/profile",
          {
            headers: {
              Authorization: "Bearer " + localStorage.getItem("token"),
            },
          }
        );
        // console.log(Cuser);
        setCurrentUser(Cuser.data.user);

        localStorage.setItem("user", JSON.stringify(Cuser.data.user));
        return { result: "success", msg: "" };
      } else if (res.data.result == "failed") {
        return {
          result: "failed",
          msg: res.data.reason,
        };
      }
    } catch (err) {
      console.log(err);
    }
  };

  const logout = async (inputs) => {
    axios.post(
      "https://be-web.onrender.com/user/logout",
      {},
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
    );

    setCurrentUser(null);
    settoken(null);
    localStorage.removeItem("user");
    localStorage.removeItem("token");
  };
  const follow = async (userId) => {
    await axios.put(
      "https://be-web.onrender.com/user/follow/" + userId,
      {},
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
    );
    const { data } = await axios.get(
      "https://be-web.onrender.com/user/profile",
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
    );
    // setCurrentUser(data.user | currentUser);
  };
  useEffect(() => {
    localStorage.setItem("user", JSON.stringify(currentUser));
  }, [currentUser]);
  useEffect(() => {
    localStorage.setItem("token", token);
  }, [token]);
  return (
    <AuthContext.Provider value={{ currentUser, token, login, logout }}>
      {children}
    </AuthContext.Provider>
  );
};
