import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import LeftBar from "./components/leftBar/LeftBar";
import { AuthContextProvider } from "./context/authContext";
import { DarkModeContextProvider } from "./context/darkModeContext";
import {
  LeftBarContextProvider,
} from "./context/leftbarContext";
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <DarkModeContextProvider>
      <AuthContextProvider>
        <LeftBarContextProvider>
          <App />
        </LeftBarContextProvider>
      </AuthContextProvider>
    </DarkModeContextProvider>
  </React.StrictMode>
);
