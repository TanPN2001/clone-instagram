import axios from "axios";

const makeRequest = axios.create({
  baseURL: "https://be-web.onrender.com",
});

// 
makeRequest.defaults.headers = {
  "Cache-Control": "no-cache",
  Pragma: "no-cache",
  Expires: "0"
};
export { makeRequest };
