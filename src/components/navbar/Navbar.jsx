import "./navbar.scss";

import { useWindowDimensions } from "../../hooks/useWindowDimension";
import { ResizeObserver } from "@juggle/resize-observer";
import useMeasure from "react-use-measure";
import Search from "../../components/search/Search";
import HomeOutlinedIcon from "@mui/icons-material/HomeOutlined";
import DarkModeOutlinedIcon from "@mui/icons-material/DarkModeOutlined";
import WbSunnyOutlinedIcon from "@mui/icons-material/WbSunnyOutlined";
import GridViewOutlinedIcon from "@mui/icons-material/GridViewOutlined";
import NotificationsOutlinedIcon from "@mui/icons-material/NotificationsOutlined";
import EmailOutlinedIcon from "@mui/icons-material/EmailOutlined";
import PersonOutlinedIcon from "@mui/icons-material/PersonOutlined";
import SearchOutlinedIcon from "@mui/icons-material/SearchOutlined";
import LogoutIcon from "@mui/icons-material/Logout";
import { Link, useNavigate } from "react-router-dom";
import { useContext, useState, useRef } from "react";
import { DarkModeContext } from "../../context/darkModeContext";
import { AuthContext } from "../../context/authContext";
import { Input } from "@mui/material";


const Navbar = () => {
  const { toggle, darkMode } = useContext(DarkModeContext);
  const { currentUser, logout } = useContext(AuthContext);

  const navigate = useNavigate();
  // console.log(currentUser);

  const hanleClick = async (e) => {
    e.preventDefault();

    try {
      await logout();
      navigate("/login");
    } catch (err) {
      console.log("Logout failed with error:" + err);
    }
  };

  return (
    <div className="navbar">
      <div className="left">
        <Link to="/" style={{ textDecoration: "none" }}>
          <span>PT7 MOMENT</span>
        </Link>
        <HomeOutlinedIcon style={{cursor: 'pointer'}} onClick={()=>{
          navigate('/')
        }} />
        {darkMode ? (
          <WbSunnyOutlinedIcon  style={{cursor: 'pointer'}} onClick={toggle} />
        ) : (
          <DarkModeOutlinedIcon  style={{cursor: 'pointer'}} onClick={toggle} />
        )}
        <GridViewOutlinedIcon   style={{cursor: 'pointer'}}/>
        <div className="search"  style={{cursor: 'pointer'}} style={{ border: "none" }}>
          <SearchOutlinedIcon  style={{cursor: 'pointer'}} />
          <SearchBar  style={{cursor: 'pointer'}} />
        </div>
      </div>
      <div className="right">
        <EmailOutlinedIcon style={{cursor: 'pointer'}} />
        <NotificationsOutlinedIcon style={{cursor: 'pointer'}} />
        <div className="user" style={{cursor: 'pointer'}} onClick={()=>{
          //console.log(currentUser);
          navigate(`/profile/${currentUser._id}`)
        }}>
          {currentUser.avatar ? (
            <img src={currentUser.avatar} alt="avatar" />
          ) : (
            <img src={"/default.png"} />
          )}

          <span>{currentUser.fullname}</span>
        </div>
        <LogoutIcon onClick={hanleClick} />
      </div>
    </div>
  );
};

export function SearchBar() {
  const [showSearch, setShowSearch] = useState(true)
  const [query, setQuery] = useState();
  const { darkMode } = useContext(DarkModeContext)
  const [ref, bounds] = useMeasure({ polyfill: ResizeObserver });
  // const { height } = useWindowDimensions();
  return (
    <>
      <Input
        ref={ref}
        type="text"
        placeholder="Search..."
        value={query}
        onChange={(e) => {
          setQuery(e.target.value);
        }}
      // onBlur={() => {
      //   setShowSearch(false)
      // }}
      // onFocus={() => {
      //   setShowSearch(true)
      // }}
      />
      {query && (
        <Search
          query={query}
          style={{
            backgroundColor: !darkMode ? "white" : "black",
            position: "absolute",
            // zIndex: "1",
            display: showSearch ? "inline-block" : "none",
            // right: `${bounds.width - 40}px`,
            // right: "20px",
            left: "331px",
            borderRadius:10 ,
            top: `${50}px`,
            width: `${bounds.width}px`,
            maxHeight: "50vh",
            overflowY: "hidden",
            overflowX: "hidden",
          }}
        />
      )}
    </>
  );
}
export default Navbar;
