import { useContext, useEffect, useState } from "react";
import Button from "@mui/material/Button";
import { styled } from "@mui/material/styles";
import TextsmsOutlinedIcon from "@mui/icons-material/TextsmsOutlined";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogActions from "@mui/material/DialogActions";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import {

  ImageList,
  ImageListItem,
} from "@mui/material";
import { makeRequest } from "../../axios";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import PostImage from "../postImage/postImage";
import "./ModalReportPost.scss";
import { AuthContext } from "../../context/authContext";
import { Link } from "react-router-dom";
import moment from "moment";

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

function BootstrapDialogTitle(props) {
  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: "absolute",
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
}

export default function ReportModalPost({ open, setOpen, post }) {
  const handleClose = () => {
    setOpen(false);
  };

  const {data } = useQuery(["post"], () =>
    makeRequest.get(`/post/${post.reportId}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
  );

  console.log(data?.data?.post);
  //const post = data?.data?.post;

  return (
    <div>
      <BootstrapDialog
        fullWidth
        maxWidth="md"
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <BootstrapDialogTitle
          id="customized-dialog-title"
          onClose={handleClose}
        >
          Báo cáo
        </BootstrapDialogTitle>

        <div>
          <PostPR post={data?.data?.post}></PostPR>
        </div>

        <DialogActions>
          <Button onClick={handleClose}>Hủy</Button>
        </DialogActions>
      </BootstrapDialog>
    </div>
  );
}

const PostPR = ({ post, showAction = true }) => {
  // console.log(post.likes)

  const [openShare, setOpenShare] = useState(false);
  const [openUpdate, setOpenUpdate] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);
  const [openReport, setOpenReport] = useState(false);
  const [commentOpen, setCommentOpen] = useState(false);
  const [menuOpen, setMenuOpen] = useState(false);
  const [user, setUser] = useState({
    name: "",
    avatar: "",
  });
  const { currentUser } = useContext(AuthContext);
  // console.log(currentUser.id)
  useEffect(() => {
    makeRequest
      .get(`user/${post.userId}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
      .then((res) => {
        setUser(res.data.user);
      })
      .catch((error) => {
        throw new Error(error)
      });
  }, [post.userId]);

  const queryClient = useQueryClient();

  // const likes = post.likes.length
  const deleteMutation = useMutation(
    (postId) => {
      return makeRequest
        .delete("post/" + postId, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        })
        .then((res) => {});
    },
    {
      onSuccess: () => {
        // Invalidate and refetch
        queryClient.invalidateQueries(["posts"]);
      },
    }
  );

  const likeMutation = useMutation(
    (postId) => {
      return makeRequest.post(
        "/post/like/" + postId,
        {},
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
    },
    {
      onSuccess: (res) => {
        // Invalidate and refetch

        queryClient.invalidateQueries(["posts"]);
      },
    }
  );

  const handleLike = () => {
    likeMutation.mutate(post._id);
  };
  const handleDelete = () => {
    deleteMutation.mutate(post._id);
  };

  return (
    <>
      <div className="post">
        <div className="container">
          <div className="user">
            <div className="userInfo">
              <img src={user.avatar} alt="" />
              <div className="details">
                <Link
                  to={`/profile/${post.userId}`}
                  style={{ textDecoration: "none", color: "inherit" }}
                >
                  <span className="name">{user.fullname}</span>
                </Link>
                <span className="date">{moment(post.createdAt).fromNow()}</span>
              </div>
            </div>

            <Button
              style={{ color: "red" }}
              onClick={() => {
                handleDelete();
              }}
            >
              Delete
            </Button>
          </div>
          <div className="content">
            <p>{post.content}</p>
            {post.images && post.images[0] && (
              // <img src={post.images[0]} alt="" />
              <ImageList sx={{ maxHeight: 600 }}>
                {post.images.map((image, id) => (
                  <ImageListItem>
                    <PostImage url={image} />
                  </ImageListItem>
                ))}
              </ImageList>
            )}
          </div>
          {showAction && (
            <div className="info">
              <div
                className="item"
                onClick={() => setCommentOpen(!commentOpen)}
              >
                <TextsmsOutlinedIcon />
                See Comments
              </div>
            </div>
          )}

          {commentOpen && (
            <div className="commentRP">
              {post.comments.map((comment) => (
                <div className="comment">
                  <img src={comment?.avatar} alt="" />
                  <div className="info">
                    <span>{comment?.name}</span>
                    <p>{comment?.comment}</p>
                  </div>
                  <span className="date">
                    {moment(comment?.date).fromNow()}
                  </span>
                </div>
              ))}
            </div>
          )}
        </div>
      </div>
      {/* <ShareModal open={openShare} setOpen={setOpenShare} postId={post._id} />
        <UpdatePostModal open={openUpdate} setOpen={setOpenUpdate} post={post} />
        <DeletePostModal open={openDelete} setOpen={setOpenDelete} post={post} />
        <ReportModal open={openReport} setOpen={setOpenReport} post={post}></ReportModal> */}
    </>
  );
};
