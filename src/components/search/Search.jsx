import { useState, useEffect, useContext } from "react";
import axios from "axios";
import {
  Avatar,
  List,
  ListItem,
  ListItemAvatar,
  ListItemButton,
  ListItemIcon,
  ListItemText,
} from "@mui/material";
import { AuthContext } from "../../context/authContext";
import { Link } from "react-router-dom"

export default function Search({ query, style }) {
  const { token } = useContext(AuthContext);
  const [searchResult, setSearchResult] = useState();

  useEffect(() => {
    console.log("Bearer " + token);
    axios
      .get(
        `https://be-web.onrender.com/user/search?q=${query}&limit=10&page=1`,
        {
          headers: {
            Authorization: "Bearer " + token,
          },
        }
      )
      .then((res) => {
        setSearchResult(res.data);
        console.log(res.data);
        // alert(res.data);
      })
      .catch((err) => {
        console.log("err", err);
        // alert(err);
      });
  }, [query]);
  return (
    <List style={style}>
      {searchResult &&
        searchResult.user.map((value, index) => {
          return <SearchUnit user={value} key={index} />;
        })}
    </List>
  );
}
export function SearchUnit({ user }) {
  return (
    <ListItem
      component={Link}
      to={`profile/${user._id}`}
      disablePadding
      style={{
        margin: "10px",
      }}
    >

      <ListItemButton>
        <ListItemAvatar>
          {" "}
          {user.avatar ? (
            <img
              src={user.avatar}
              style={{
                width: "30px",
                height: "30px",
                borderRadius: "0px",
              }}
            />
          ) : (
            <img
              src="/default.png"
              style={{
                width: "30px",
                height: "30px",
                borderRadius: "0px",
              }}
            />
          )}
        </ListItemAvatar>
        <ListItemText primary={user.fullname}></ListItemText>
      </ListItemButton>
    </ListItem>
  );
}
