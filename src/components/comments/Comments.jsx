import { useContext, useEffect, useState } from "react";
import "./comments.scss";
import { AuthContext } from "../../context/authContext";
import { useQuery, useMutation, useQueryClient } from "@tanstack/react-query";
import { makeRequest } from "../../axios";
import moment from "moment";
import EditCommentModal from "../editCommentModal/editCommentModal";
import { Button } from "@mui/material";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
const Comments = ({ postId, postComment }) => {
  const [comment, setComment] = useState("");
  const { currentUser } = useContext(AuthContext);

  const queryClient = useQueryClient();

  const mutation = useMutation(
    (newComment) => {
      return makeRequest.post("post/comment", newComment, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
    },
    {
      onSuccess: (res) => {
        console.log(res);
        // Invalidate and refetch
        queryClient.invalidateQueries(["posts"]);
      },
    }
  );

  const handleClick = async (e) => {
    e.preventDefault();
    mutation.mutate({ comment: comment, postId });
    setComment("");
  };

  return (
    <div className="comments">
      <div className="write">
        <img src={currentUser.avatar} alt="" />
        <input
          type="text"
          maxLength={100}
          placeholder="write a comment"
          value={comment}
          onChange={(e) => setComment(e.target.value)}
        />
        <button onClick={handleClick}>Send</button>
      </div>
      {postComment.map((comment, id) => (
        <Comment comment={comment} key={id} postId={postId} />
      ))}
    </div>
  );
};

export default Comments;
function Comment({ comment, postId }) {
  const [open, setOpen] = useState(false);

  const [user, setUser] = useState({
    avatar: "",
    fullname: "",
  });
  useEffect(() => {
    // console.log(comment)
    makeRequest
      .get(`/user/${comment.user}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
      .then((res) => {
        setUser(res.data.user);
      });
  }, [comment.user]);

  return (
    <div className="comment">
      <img src={user.avatar} alt="" />
      <div className="info">
        <span>
          {user.fullname}{" "}
          <span className="date">{moment(comment.date).fromNow()}</span>
        </span>

        <p>{comment.comment}</p>
      </div>
      {/* <span className="date">{moment(comment.date).fromNow()}</span> */}
      <EditCommentModal
        open={open}
        setOpen={setOpen}
        comment={comment}
        postId={postId}
      />

      {JSON.parse(localStorage.getItem("user"))._id === comment.user && (
        <MoreHorizIcon
          variant="contained"
          onClick={() => {
            setOpen(true);
          }}
          style={{cursor: 'pointer' }}
        >
        </MoreHorizIcon>
      )}
    </div>
  );
}
