import { useQuery, useQueryClient } from "@tanstack/react-query";
import { makeRequest } from "../../axios";
import moment from "moment/moment";
import { Bar, Line, Pie } from "react-chartjs-2";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Colors,
  PointElement,
  LineElement,
  ArcBorderRadius,
  Legend,
  ArcElement,
} from "chart.js";
ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  PointElement,
  LineElement,
  ArcElement,
  Colors,
  Title,
  Tooltip,
  Legend
);
const option = {
  reponsive: true,
  plugins: {
    title: {
      display: true,
      text: "Tỉ lệ giữa các trạng thái của bài viết",
    },
  },
  scales: {
    y: {
      beginAtZero: true,
    },
  },
};
export default function Chart({}) {
  return (
    <>
      <UserChart />
      <br></br>
      <br></br>
      <br></br>
      <PostChart />
    </>
  );
}

function UserChart() {
  const { isLoading, error, data } = useQuery(["chart", "user"], () =>
    makeRequest.get("user/list-user", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
  );
  let users = [];
  if (!(isLoading || error)) {
    users = data.data.user
      .sort((user1, user2) => {
        return moment(user1.createdAt) > moment(user2.createdAt);
      })
      .map((user) => {
        return {
          ...user,
          createdAt: moment(user.createdAt).format("DD/MM/yyyy"),
        };
      });
  }

  return (
    <>
      {isLoading ? (
        <p>Loading...</p>
      ) : error ? (
        <p></p>
      ) : (
        <>
          <header>Người dùng</header>
          <NumOfNewUserPerDay users={users} />
          {/* <NumOfAllUserPerDay users={users} /> */}
          <RatioBetweenUserAndAdmin users={users} />
          {/* <AverageFollowerOfAnUser users={users} /> */}
        </>
      )}
    </>
  );
}

function NumOfNewUserPerDay({ users }) {
  let label = [];
  for (let i = 0; i < 7; i++) {
    label.push(moment().subtract(i, "day").format("DD/MM/yyyy"));
  }
  label = label.reverse();
  //console.log(users);
  // const label = [...new Set(users.map((user) => user.createdAt))];
  //console.log(label);
  //   console.log(
  //     label.map((lb) => {
  //       return {
  //         data: 1,
  //       };
  //     })
  //   );
  return (
    <span
      style={{
        minWidth: "550px",
        display: "inline-block",
        marginLeft: "100px",
      }}
    >
      <Bar
        options={{
          reponsive: true,
          plugins: {
            title: {
              display: true,
              text: "Thống kê số người đăng kí mới theo ngày",
            },
          },
          scales: {
            y: {
              beginAtZero: true,
            },
          },
        }}
        data={{
          labels: label,
          datasets: [
            {
              label: "Số người đăng kí mới theo ngày",
              data: label.map(
                (lb) => users.filter((user) => user.createdAt == lb).length
              ),
            },
          ],
        }}
      />
    </span>
  );
}
function NumOfAllUserPerDay({ users }) {
  let label = [];
  for (let i = 0; i < 7; i++) {
    label.push(moment().subtract(i, "day").format("DD/MM/yyyy"));
  }
  label = label.reverse();
  // console.log(moment(moment().format("DD/MM/yyyy")).add(1, "day"));
  // const label = [...new Set(users.map((user) => user.createdAt))];
  //   console.log(label);
  //   console.log(
  //     label.map(
  //       (lb) =>
  //         users.filter(
  //           (user) =>
  //             moment(user.createdAt) < moment(lb) ||
  //             moment(user.createdAt) == moment(lb)
  //         ).length
  //     )
  //   );

  return (
    <span
      style={{
        minWidth: "550px",
        display: "inline-block",
        marginLeft: "100px",
      }}
    >
      <Line
        options={option}
        data={{
          labels: label,
          datasets: [
            {
              label: "",
              data: label.map(
                (lb) =>
                  users.filter((user) =>
                    moment(user.createdAt, "DD/MM/yyyy").isSameOrBefore(
                      moment(lb, "DD/MM/yyyy")
                    )
                  ).length
              ),
            },
          ],
        }}
      />
    </span>
  );
}
function RatioBetweenUserAndAdmin({ users }) {
  return (
    <span
      style={{
        minWidth: "550px",
        display: "inline-block",
        marginLeft: "100px",
      }}
    >
      <Pie
        options={{
          responsive: true,
          maintainAspectRatio: true,
          plugins: {
            title: {
              display: true,
              text: "Tỉ lệ người quản lý và người dùng",
            },
          },
        }}
        data={{
          labels: ["Người quản lý", "Người dùng"],
          datasets: [
            {
              label: "",
              data: [0, 1].map(
                (role) => users.filter((user) => user.role == role).length
              ),
            },
          ],
        }}
      />
    </span>
  );
}
function AverageFollowerOfAnUser({ users }) {
  let avg = 0;
  users.forEach((value) => {
    //console.log(value.followers.length)
    avg += value.followers.length;
  });
  // console.log(avg)
  avg = avg / users.length;

  return (
    <p>
      Trung bình mỗi người dùng được theo dõi và được theo dõi bởi{" "}
      {avg.toPrecision(2)} người{" "}
    </p>
  );
}


function PostChart() {
  const { isLoading, error, data } = useQuery(["chart", "post"], () =>
    makeRequest.get("post/analytics", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
  );
  //console.log(data);
  let posts = [];
  if (!(isLoading || error))
    posts = data.data.post.sort(
      (postA, postB) => moment(postA.createdAt) > moment(postB.createdAt)
    );

  //console.log(posts);
  return (
    <div>
      <NumberOfNewPostPerDay posts={posts} />
      {/* <NumberOfAllPostPerDay posts={posts} /> */}
      <RatioBetweenStatusOfPost posts={posts} />
    </div>
  );
}
function NumberOfNewPostPerDay({ posts }) {
  let labels = [];
  for (let i = 0; i < 7; i++) {
    labels.push(moment().subtract(i, "day").format("DD/MM/yyyy"));
  }
  labels = labels.reverse();
  return (
    <span
      style={{
        minWidth: "550px",
        display: "inline-block",
        marginLeft: "100px",
      }}
    >
      <Bar
        options={{
          responsive: true,
          maintainAspectRatio: true,
          plugins: {
            title: {
              display: true,
              text: "Thống kê số bài viết theo ngày",
            },
          },
        }}
        data={{
          labels: labels,

          datasets: [
            {
              label: "Bài viết",
              data: labels.map(
                (label) =>
                  posts.filter((post) =>
                    moment(post.createdAt).isSame(
                      moment(label, "DD/MM/yyyy"),
                      "day"
                    )
                  ).length
              ),
            },
          ],
        }}
      />
    </span>
  );
}
function NumberOfAllPostPerDay({ posts }) {
  let labels = [];
  for (let i = 0; i < 7; i++) {
    labels.push(moment().subtract(i, "day").format("DD/MM/yyyy"));
  }
  labels = labels.reverse();
  return (
    <span
      style={{
        minWidth: "550px",
        display: "inline-block",
        marginLeft: "100px",
      }}
    >
      <Line
        options={option}
        data={{
          labels: labels,

          datasets: [
            {
              label: "Bài viết",
              data: labels.map(
                (label) =>
                  posts.filter((post) =>
                    moment(post.createdAt).isSameOrBefore(
                      moment(label, "DD/MM/yyyy"),
                      "day"
                    )
                  ).length
              ),
            },
          ],
        }}
      />
    </span>
  );
}

function RatioBetweenStatusOfPost({ posts }) {
  return (
    <span
      style={{
        minWidth: "550px",
        display: "inline-block",
        marginLeft: "100px",
      }}
    >
      <Pie
        // options={option}
        options={{
          responsive: true,
          maintainAspectRatio: true,
          plugins: {
            title: {
              display: true,
              text: "Tỉ lệ giữa các trạng thái của bài viết",
            },
          },
        }}
        data={{
          labels: ["Mọi người", "Người theo dõi", "Chỉ mình tôi"],
          datasets: [
            {
              label: "Bài viết",
              data: ["public", "follower", "private"].map(
                (status) => posts.filter((post) => post.status == status).length
              ),
            },
          ],
        }}
      />
    </span>
  );
}
