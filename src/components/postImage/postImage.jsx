import { Dialog, DialogContent } from "@mui/material"
import { useState } from "react"
export default function PostImage({ url }) {
    const [open, setOpen] = useState(false)
    return (
        <>
            <img src={url} onClick={() => {
                setOpen(true);

            }} />
            <Dialog open={open} onClose={() => { setOpen(false) }}>
                <DialogContent sx={{ p: 0 }}>
                    {/* <img src={url} style={{ maxHeight: "100vh", maxWidth: "100vh" }} /> */}
                    <img src={url} alt="" style={{ maxHeight: "85vh", height: "auto", width: "100%" }} />
                    {/* 1111 */}
                </DialogContent>
            </Dialog>
        </>
    )
}