import "./leftBar.scss";
import DynamicFeedIcon from "@mui/icons-material/DynamicFeed";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import FavoriteIcon from "@mui/icons-material/Favorite";
import { AuthContext } from "../../context/authContext";
import { useContext, useState } from "react";
import AnalyticsIcon from "@mui/icons-material/Analytics";
import { Collapse } from "@mui/material";
import { Link } from "react-router-dom";
const LeftBar = () => {
  const [open, setOpen] = useState("bar");
  const { currentUser } = useContext(AuthContext);

  return (
    <>
      {/* <Collapse in={open === "bar"} orientation="horizontal" mountOnEnter unmountOnExit> */}

      <div className="leftBar">
        <div className="container">
          <div className="menu">
            <Link to={`/profile/${currentUser._id}`} className="user">
              <img src={currentUser.avatar} alt="" />
              <span>{currentUser.fullname}</span>
            </Link>

            <Link className="item" to={"/"}>
              <DynamicFeedIcon color="primary" />
              <span>Feed</span>
            </Link>

            <Link className="item" to="/followers">
              <FavoriteBorderIcon color="primary"></FavoriteBorderIcon>
              <span>Người theo dõi</span>
            </Link>
            <Link className="item" to="/following">
              <FavoriteIcon color="primary"></FavoriteIcon>
              <span>Đang theo dõi</span>
            </Link>

            {currentUser.role == 0 && (
              <Link className="item" to="/admin">
                <AnalyticsIcon color="primary"></AnalyticsIcon>
                <span>Admin</span>
              </Link>
            )}
          </div>
        </div>
      </div>
      {/* </Collapse> */}
      {/* <Collapse in={open === "bar"} orientation="horizontal" mountOnEnter unmountOnExit>
        123
      </Collapse> */}
    </>
  );
};

export default LeftBar;
