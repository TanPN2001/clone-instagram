


import { useState } from "react"
import Button from "@mui/material/Button";
import Select from "@mui/material/Select";
import { styled } from "@mui/material/styles";

import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import Typography from "@mui/material/Typography";
import { FormControl, InputLabel, MenuItem, TextField, ImageList, ImageListItem } from "@mui/material";
import { maxWidth } from "@mui/system";
import { makeRequest } from "../../axios";
import { useQueryClient, useMutation } from "@tanstack/react-query";
const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    "& .MuiDialogContent-root": {
        padding: theme.spacing(2),
    },
    "& .MuiDialogActions-root": {
        padding: theme.spacing(1),
    },
}));

function BootstrapDialogTitle(props) {
    const { children, onClose, ...other } = props;

    return (
        <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
            {children}
            {onClose ? (
                <IconButton
                    aria-label="close"
                    onClick={onClose}
                    sx={{
                        position: "absolute",
                        right: 8,
                        top: 8,
                        color: (theme) => theme.palette.grey[500],
                    }}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </DialogTitle>
    );
}

export default function DeletePostModal({ open, setOpen, post }) {
    const queryClient = useQueryClient()

    const handleClose = () => {
        setOpen(false);
    };
    const deleteMutation = useMutation(
        (postId) => {
            return makeRequest.delete("/post/" + postId, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`
                }
            });
        },
        {
            onSuccess: () => {
                // Invalidate and refetch
                queryClient.invalidateQueries(["posts"]);
                setOpen(false)
            },
        }
    );
    const handleDelete = () => {
        deleteMutation.mutate(post._id);
    };

    return (
        <div>

            <BootstrapDialog

                fullWidth
                maxWidth="sm"
                onClose={handleClose}
                aria-labelledby="customized-dialog-title"
                open={open}
            >
                <BootstrapDialogTitle
                    id="customized-dialog-title"
                    onClose={handleClose}
                >
                    Xóa
                </BootstrapDialogTitle>
                <DialogContent dividers>
                    <Typography> Bạn có chắc muốn xóa bài viết không?</Typography>
                </DialogContent>

                <DialogActions>
                    <Button onClick={handleClose}>
                        Hủy
                    </Button>
                    <Button color="error" variant="contained" onClick={handleDelete}>
                        Xóa
                    </Button>
                </DialogActions>
            </BootstrapDialog>
        </div>
    );
}
