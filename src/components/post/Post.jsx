import "./post.scss";
import FavoriteBorderOutlinedIcon from "@mui/icons-material/FavoriteBorderOutlined";
import FavoriteOutlinedIcon from "@mui/icons-material/FavoriteOutlined";
import TextsmsOutlinedIcon from "@mui/icons-material/TextsmsOutlined";
import ShareOutlinedIcon from "@mui/icons-material/ShareOutlined";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import UpdatePostModal from "../updatePostModal/updatePostModal";
import { Link } from "react-router-dom";
import Comments from "../comments/Comments";
import ShareModal from "../shareModal/ShareModal";
import { useState } from "react";
import moment from "moment";
import { useQuery, useQueryClient, useMutation } from "@tanstack/react-query";
import { makeRequest } from "../../axios";
import { useContext, useEffect } from "react";
import { AuthContext } from "../../context/authContext";
import axios from "axios";
import { Dialog, DialogContent, ImageList, ImageListItem } from "@mui/material";
import DeletePostModal from "../deletePostModal/deletePostModal";
import PostImage from "../postImage/postImage";
import ReportModal from "../reportModal/reportModal";

const Post = ({ post, showAction = true }) => {
  // console.log(post.likes)

  const [openShare, setOpenShare] = useState(false);
  const [openUpdate, setOpenUpdate] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);
  const [openReport, setOpenReport] = useState(false);
  const [commentOpen, setCommentOpen] = useState(false);
  const [menuOpen, setMenuOpen] = useState(false);
  const [user, setUser] = useState({
    name: "",
    avatar: "",
  });
  const { currentUser } = useContext(AuthContext);
  // console.log(currentUser.id)
  useEffect(() => {
    makeRequest
      .get(`user/${post.userId}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
      .then((res) => {
        setUser(res.data.user);
      });
  }, [post.userId]);

  const queryClient = useQueryClient();

  // const likes = post.likes.length
  const deleteMutation = useMutation(
    (postId) => {
      return makeRequest
        .delete("post/" + postId, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        })
        .then((res) => {});
    },
    {
      onSuccess: () => {
        // Invalidate and refetch
        queryClient.invalidateQueries(["posts"]);
      },
    }
  );

  const likeMutation = useMutation(
    (postId) => {
      return makeRequest.post(
        "/post/like/" + postId,
        {},
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
    },
    {
      onSuccess: (res) => {
        // Invalidate and refetch

        queryClient.invalidateQueries(["posts"]);
      },
    }
  );

  const handleLike = () => {
    likeMutation.mutate(post._id);
  };
  const handleDelete = () => {
    deleteMutation.mutate(post._id);
  };

  return (
    <>
      <div className="post">
        <div className="container">
          <div className="user">
            <div className="userInfo">
              <img src={user.avatar} alt="" />
              <div className="details">
                <Link
                  to={`/profile/${post.userId}`}
                  style={{ textDecoration: "none", color: "inherit" }}
                >
                  <span className="name">{user.fullname}</span>
                </Link>
                <span className="date">{moment(post.createdAt).fromNow()}</span>
              </div>
            </div>

            <div className="dropdown">
              <button
                className="btn"
                type="button"
                id="dropdownMenuButton"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
                style={{ backgroundColor: "transparent" }}
              >
                <MoreHorizIcon></MoreHorizIcon>
              </button>
              <div
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton"
              >
                <a
                  className="dropdown-item"
                  href="#"
                  onClick={() => {
                    setOpenReport(true);
                  }}
                >
                  Báo cáo
                </a>
                {post.userId === currentUser._id && (
                  <a
                    class="dropdown-item"
                    href="#"
                    onClick={() => {
                      setOpenDelete(true);
                    }}
                  >
                    Xóa
                  </a>
                )}
                {post.userId === currentUser._id && (
                  <a
                    class="dropdown-item"
                    href="#"
                    onClick={() => {
                      setOpenUpdate(true);
                    }}
                  >
                    Cập nhật
                  </a>
                )}
              </div>
            </div>
          </div>
          <div className="content">
            <p>{post.content}</p>
            {post.images && post.images[0] && (
              // <img src={post.images[0]} alt="" />
              <ImageList sx={{ maxHeight: 600 }}>
                {post.images.map((image, id) => (
                  <ImageListItem>
                    <PostImage url={image} />
                  </ImageListItem>
                ))}
              </ImageList>
            )}
          </div>
          {showAction && (
            <div className="info">
              <div className="item">
                {post.likes.includes(currentUser._id) ? (
                  <FavoriteOutlinedIcon
                    style={{ color: "red" }}
                    onClick={handleLike}
                  />
                ) : (
                  <FavoriteBorderOutlinedIcon onClick={handleLike} />
                )}
                {post.likes?.length} Likes
              </div>
              <div
                className="item"
                onClick={() => setCommentOpen(!commentOpen)}
              >
                <TextsmsOutlinedIcon />
                See Comments
              </div>
              <div
                className="item"
                onClick={() => {
                  setOpenShare(true);
                }}
              >
                <ShareOutlinedIcon />
                Share
              </div>
            </div>
          )}

          {commentOpen && (
            <Comments postId={post._id} postComment={post.comments} />
          )}
        </div>
      </div>
      <ShareModal open={openShare} setOpen={setOpenShare} postId={post._id} />
      <UpdatePostModal open={openUpdate} setOpen={setOpenUpdate} post={post} />
      <DeletePostModal open={openDelete} setOpen={setOpenDelete} post={post} />
      <ReportModal open={openReport} setOpen={setOpenReport} post={post} type="POST"></ReportModal>
    </>
  );
};

export default Post;
