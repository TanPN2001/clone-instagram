import { useContext, useEffect, useState } from "react";
import Button from "@mui/material/Button";
import Select from "@mui/material/Select";
import { styled } from "@mui/material/styles";
import FavoriteBorderOutlinedIcon from "@mui/icons-material/FavoriteBorderOutlined";
import FavoriteOutlinedIcon from "@mui/icons-material/FavoriteOutlined";
import TextsmsOutlinedIcon from "@mui/icons-material/TextsmsOutlined";
import ShareOutlinedIcon from "@mui/icons-material/ShareOutlined";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import Typography from "@mui/material/Typography";
import {
  FormControl,
  InputLabel,
  MenuItem,
  TextField,
  ImageList,
  ImageListItem,
} from "@mui/material";
import { maxWidth } from "@mui/system";
import { makeRequest } from "../../axios";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { AuthContext } from "../../context/authContext";
import { Link } from "react-router-dom";
import moment from "moment";
import PostImage from "../postImage/postImage";
const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

function BootstrapDialogTitle(props) {
  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: "absolute",
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
}

export default function ReportModal({ open, setOpen, post, type }) {
  const queryClient = useQueryClient();
  const [content, setContent] = useState('');
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <BootstrapDialog
        fullWidth
        maxWidth="md"
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <BootstrapDialogTitle
          id="customized-dialog-title"
          onClose={handleClose}
        >
          Báo cáo
        </BootstrapDialogTitle>
        <DialogContent dividers></DialogContent>
        <FormControl variant="standard">
          <TextField
            sx={{
              mx: "20px",
            }}
            multiline
            variant="outlined"
            label="Lý do báo cáo"
            value={content}
            onChange={(e) => {
              setContent(e.target.value);
            }}
          ></TextField>
        </FormControl>
        <DialogActions>
          <Button onClick={handleClose}>Hủy</Button>
          <Button
            color="primary"
            variant="contained"
            onClick={() => {
              makeRequest
                .post(
                  "/report/create",
                  {
                    description: content,
                    type: type,
                    reportId: post._id
                  },
                  {
                    headers: {
                      Authorization: `Bearer ${localStorage.getItem("token")}`,
                    },
                  }
                )
                .then((res) => {
                  queryClient.invalidateQueries(["posts"]);
                  setOpen(false);
                });
            }}
          >
            Báo cáo
          </Button>
        </DialogActions>
      </BootstrapDialog>
    </div>
  );
}

