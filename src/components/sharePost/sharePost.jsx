import "./sharePost.scss";
import FavoriteBorderOutlinedIcon from "@mui/icons-material/FavoriteBorderOutlined";
import FavoriteOutlinedIcon from "@mui/icons-material/FavoriteOutlined";
import TextsmsOutlinedIcon from "@mui/icons-material/TextsmsOutlined";
import ShareOutlinedIcon from "@mui/icons-material/ShareOutlined";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import UpdatePostModal from "../updatePostModal/updatePostModal";
import DeletePostModal from "../deletePostModal/deletePostModal"
import { Link } from "react-router-dom";
import Comments from "../comments/Comments";
import ShareModal from "../shareModal/ShareModal";
import { useState } from "react";
import moment from "moment";
import { useQuery, useQueryClient, useMutation } from "@tanstack/react-query";
import { makeRequest } from "../../axios";
import { useContext, useEffect } from "react";
import { AuthContext } from "../../context/authContext";
import axios from "axios";
import Post from "../post/Post"
import { Divider, ListItem, ImageList, ImageListItem } from "@mui/material";
const SharePost = ({ post, showAction = true }) => {
    // console.log(post.likes)
    const [openShare, setOpenShare] = useState(false)
    const [openDelete, setOpenDelete] = useState(false)
    const [commentOpen, setCommentOpen] = useState(false);
    const [menuOpen, setMenuOpen] = useState(false);
    const [openEdit, setOpenEdit] = useState(false)
    const [user, setUser] = useState({
        name: "",
        avatar: ""
    })
    const [sharePost, setSharePost] = useState()
    const { currentUser } = useContext(AuthContext);
    // console.log(currentUser.id)
    useEffect(() => {
        makeRequest.get(`user/${post.userId}`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }
        }).then((res) => {
            console.log(res.data)
            setUser(res.data.user)
        })
        makeRequest.get(`post/${post.sharePostId}`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }
        }).then((res) => {
            setSharePost(res.data.post)

        })
    }, [post.userId, post.sharePostId])

    const queryClient = useQueryClient();

    // const likes = post.likes.length


    const likeMutation = useMutation(
        (postId) => {
            console.log(
                postId
            )
            return makeRequest.post("/post/like/" + postId, {

            }, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`
                }
            });
        },
        {
            onSuccess: (res) => {
                // Invalidate and refetch
                console.log(res)
                queryClient.invalidateQueries(["posts"]);
            },
        }
    );

    const handleLike = () => {
        likeMutation.mutate(post._id)
    }


    return (<>

        <div className="post">
            <div className="container">
                <div className="user">
                    <div className="userInfo">
                        <img src={user.avatar} alt="" />
                        <div className="details">
                            <Link
                                to={`/profile/${post.userId}`}
                                style={{ textDecoration: "none", color: "inherit" }}
                            >
                                <span className="name">{user.fullname} <span style={{ color: "gray" }}> đã chia sẻ</span></span>
                            </Link>
                            <span className="date">{moment(post.createdAt).fromNow()}</span>
                        </div>
                    </div>
                    {showAction && post.userId === currentUser._id &&
                        <MoreHorizIcon onClick={() => setMenuOpen(!menuOpen)} />
                    }
                    {menuOpen && post.userId === currentUser._id && (
                        <>
                            <button onClick={() => { setOpenDelete(true) }} key={1} className="delete">xóa</button>
                            <button onClick={() => { setOpenEdit(true) }} key={2} className="updateButton">cập nhật</button>

                        </>
                        // <List className>
                        //     <ListItem>xóa</ListItme>
                        // </List>
                    )}
                </div>
                <div className="content">
                    <p>{post.content}</p>
                    {
                        (post.images && post.images[0]) &&
                        // <img src={post.images[0]} alt="" />
                        <ImageList sx={{ maxHeight: 600 }}>
                            {
                                post.images.map((image) => (<ImageListItem>
                                    <img src={image}></img>
                                </ImageListItem>))
                            }
                        </ImageList>
                    }

                </div>
                {/* <Divider></Divider> */}
                <div className="sharePost">
                    {
                        sharePost?.userId && (sharePost.sharePostId ? <SharePost post={sharePost} showAction={false} /> : <Post post={sharePost} showAction={false}></Post>)
                    }

                </div>
                {showAction && <div className="info" >
                    <div className="item">
                        {post.likes.includes(currentUser._id) ? (
                            <FavoriteOutlinedIcon
                                style={{ color: "red" }}
                                onClick={handleLike}
                            />
                        ) : (
                            <FavoriteBorderOutlinedIcon onClick={handleLike} />
                        )}
                        {post.likes?.length} Likes
                    </div>
                    <div className="item" onClick={() => setCommentOpen(!commentOpen)}>
                        <TextsmsOutlinedIcon />
                        See Comments
                    </div>
                    <div className="item" onClick={() => { setOpenShare(true) }}>
                        <ShareOutlinedIcon />
                        Share
                    </div>
                </div>}
                {commentOpen && <Comments postId={post._id} postComment={post.comments} />}
            </div>
        </div>
        <ShareModal open={openShare} setOpen={setOpenShare} postId={post._id} />
        <UpdatePostModal open={openEdit} setOpen={setOpenEdit} post={post} />
        <DeletePostModal open={openDelete} setOpen={setOpenDelete} post={post} />
    </>);
};

export default SharePost;
