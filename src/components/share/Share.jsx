import "./share.scss";
import RoomIcon from "@mui/icons-material/Room";
import LocalOfferIcon from "@mui/icons-material/LocalOffer";
import { useContext, useState } from "react";
import { AuthContext } from "../../context/authContext";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { makeRequest } from "../../axios";
import { Select, MenuItem, FormControl, InputLabel } from "@mui/material";
import AddPhotoAlternateIcon from "@mui/icons-material/AddPhotoAlternate";
const Share = () => {
  const [file, setFile] = useState();
  const [content, setContent] = useState("");
  const [status, setStatus] = useState("public");

  // const upload = async () => {
  //   try {
  //     const formData = new FormData();
  //     formData.append("file", file);
  //     const res = await makeRequest.post("/upload", formData);
  //     return res.data;
  //   } catch (err) {
  //     console.log(err);
  //   }
  // };

  const { currentUser } = useContext(AuthContext);

  const queryClient = useQueryClient();

  const mutation = useMutation(
    (newPost) => {
      return makeRequest.post("/post/create", newPost, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
          Accept: "application/json",
          // 'Content-Type': 'multipart/form-data',
        },
      });
    },
    {
      onSuccess: (data) => {
        // Invalidate and refetch
        console.log("data", data);
        queryClient.invalidateQueries(["posts"]);
      },
    }
  );

  const handleClick = async (e) => {
    let formData = new FormData();
    formData.append("content", content);
    if (file) {
      console.log("1");
      console.log(file.length);
      for (let i = 0; i < file.length; i++) {
        console.log(2);
        formData.append("image", file[i]);

        console.log(formData.getAll("image"));
      }
    }
    // formData.append("image", file)
    // console.log(formData.get("image"))
    formData.append("status", status);
    mutation.mutate(formData);
    setContent("");
    setFile(null);
    // setStatus("public")
  };

  return (
    <div className="share">
      <div className="container">
        <div className="top">
          <div className="left">
            <img
              src={currentUser.avatar ? currentUser.avatar : "/default.png"}
              alt=""
            />
            <input
              type="text"
              placeholder={`What's on your mind ${currentUser.name}?`}
              onChange={(e) => setContent(e.target.value)}
              value={content}
            />
          </div>
          <div className="right">
            {file && file[0] && (
              <img className="file" alt="" src={URL.createObjectURL(file[0])} />
            )}
          </div>
        </div>
        <hr />
        <div className="bottom">
          <div className="left">
            <input
              type="file"
              multiple={true}
              id="file"
              style={{ display: "none" }}
              onChange={(e) => {
                setFile(e.target.files);
                // e.target.value = null
              }}
            />
            <label htmlFor="file">
              <div className="item">
                <AddPhotoAlternateIcon color="primary" />
                <span>Add Image</span>
              </div>
            </label>
            <div className="item">
              <RoomIcon color="primary" />
              <span>Add Place</span>
            </div>
            <div className="item">
              <LocalOfferIcon color="primary" />
              <span>Tag Friends</span>
            </div>

            <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }}>
              <InputLabel id="status-label">Trạng thái</InputLabel>
              <Select
                labelId="status-label"
                id="status"
                value={status}
                onChange={(e) => {
                  setStatus(e.target.value);
                }}
                label="status"
              >
                <MenuItem value={"public"}>Mọi người</MenuItem>
                <MenuItem value={"follower"}>Người theo dõi</MenuItem>
                <MenuItem value={"private"}>Chỉ mình tôi</MenuItem>
              </Select>
            </FormControl>
          </div>
          <div className="right">
            <button onClick={handleClick}>Share</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Share;
