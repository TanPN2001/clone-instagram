import Post from "../post/Post";
import "./posts.scss";
import { LinearProgress } from "@mui/material";
import { useState, useEffect, useContext } from "react";
import { useQuery } from "@tanstack/react-query";
import { makeRequest } from "../../axios";
import SharePost from "../sharePost/sharePost"
import { AuthContext } from "../../context/authContext";
const Posts = ({ userId }) => {

  // console.log(userId)
  const { currentUser } = useContext(AuthContext)

  const { isLoading, error, data: posts } = useQuery(["posts", userId], () => makeRequest.get(userId ? `/post/all-post/${userId}` : "new-feed", {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`
    }
  }).then((res) => {

    //console.log(res.data)
    const filteredData = res.data.post.filter(
      (v, i, a) => {

        return v.userId != currentUser._id
      }
    );
    return res.data.post
  }

  ))
  return (
    <div className="posts">
      {isLoading ? <LinearProgress /> : (
        error ? "error" : posts.map((post, id) => (post.sharePostId == null ? <Post post={post} key={post._id} /> : <SharePost post={post} key={post._id} />))
      )
      }
    </div>
  );
};

export default Posts;
