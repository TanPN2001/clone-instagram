import "./rightBar.scss";
import { useQuery, useQueryClient } from "@tanstack/react-query";
import { makeRequest } from "../../axios";
import { Link, useNavigate } from "react-router-dom";
import { LinearProgress, Button, Fade, Divider } from "@mui/material";
import { TransitionGroup } from "react-transition-group"
const RightBar = () => {
  const { isLoading, error, data } = useQuery(["suggestion"], () =>
    makeRequest.get("/new-feed/suggestion", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    }).then((res) => {

      return res.data.user
    })
  );
  return (
    <div className="rightBar">
      <div className="container">
        <div className="item">
          <span>Suggestions For You</span>

          {error
            ? "Something went wrong!"
            : isLoading
              ? <LinearProgress />

              : <TransitionGroup> {data.map((user, id) => <Fade><div> <User user={user} key={id} /></div></Fade>)}</TransitionGroup>}

        </div>
      </div>
    </div>
  );
};

export default RightBar;

function User({ user }) {
  const queryClient = useQueryClient()
  const navigate = useNavigate()
  return (
    <>
      <Divider />
      <div onClick={() => {
        navigate("/profile/" + user._id)
      }} className="user" style={{ textDecoration: "none", cursor: "pointer" }}>
        <div className="userInfo">
          <img
            src={user.avatar ? user.avatar : "/Default.png"}
            alt="" />
          <span>{user.fullname}</span>
        </div>
        <div className="buttons">
          {/* <button onClick={()=>{}}>follow</button> */}
          <Button variant="contained" sx={{ borderRadius: '999px 999px 999px 999px' }} onClick={(e) => {
            e.stopPropagation()

            makeRequest.put(`/user/follow/${user._id}`, {}, {
              headers: {
                Authorization: `Beare ${localStorage.getItem("token")}`
              }
            }).then((res) => {
              console.log(res)
              queryClient.invalidateQueries(["suggestion"])
              queryClient.invalidateQueries(["user"])
              queryClient.invalidateQueries(["posts"])
              queryClient.invalidateQueries(["following"])
            })
          }}> theo dõi</Button>
          {/* <button>dismiss</button> */}
        </div>
      </div>
    </>
  );
}

