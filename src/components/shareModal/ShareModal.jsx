import "./shareModal.scss";

import { useState } from "react"
import Button from "@mui/material/Button";
import Select from "@mui/material/Select";
import { styled } from "@mui/material/styles";

import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import Typography from "@mui/material/Typography";
import { FormControl, InputLabel, MenuItem, TextField } from "@mui/material";
import { maxWidth } from "@mui/system";
import { makeRequest } from "../../axios";
import { useQueryClient } from "@tanstack/react-query";

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

function BootstrapDialogTitle(props) {
  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: "absolute",
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
}

export default function ShareModal({ open, setOpen, postId }) {
  const queryClient = useQueryClient()
  const [option, setOption] = useState("public");
  const [content, setContent] = useState("")

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>

      <BootstrapDialog

        fullWidth
        maxWidth="md"
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <BootstrapDialogTitle
          id="customized-dialog-title"
          onClose={handleClose}
        >
          Chia sẻ
        </BootstrapDialogTitle>
        <DialogContent dividers></DialogContent>
        <FormControl variant="standard">
          <TextField
            sx={{
              mx: "20px"
            }}
            multiline
            variant="outlined"
            label="Chia sẻ cảm xúc của bạn"
            value={content}
            onChange={(e) => { setContent(e.target.value) }}
          ></TextField>
          <Typography
            sx={
              {
                margin: "20px"
              }
            }
            display="inline"


          >
            Bạn muốn chia sẻ với
          </Typography>


          <Select
            sx={
              {
                margin: "20px",
                mt: "0px"
              }
            }
            value={option}
            onChange={(e) => {
              setOption(e.target.value);
            }}
          >
            <MenuItem value={"public"}>Mọi người</MenuItem>
            <MenuItem value={"follower"}>Người theo dõi</MenuItem>
            <MenuItem value={"private"}>Chỉ mình bạn</MenuItem>
          </Select>
          <></>
        </FormControl>

        <DialogActions>
          <Button onClick={handleClose}>
            Quay lại
          </Button>
          <Button onClick={() => {
            makeRequest.post("/post/share/" + postId, {
              content, status: option
            }, {
              headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`
              }
            }).then((res) => {
              queryClient.invalidateQueries(["posts"])
              setOpen(false)
              setContent("")
              setOption("public")
              console.log(res)
            })
          }}>
            Chia sẻ
          </Button>
        </DialogActions>
      </BootstrapDialog>
    </div>
  );
}
