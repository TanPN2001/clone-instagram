import { useState } from "react"
import Button from "@mui/material/Button";
import Select from "@mui/material/Select";
import { styled } from "@mui/material/styles";

import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import Typography from "@mui/material/Typography";
import { FormControl, InputLabel, MenuItem, TextField, ImageList, ImageListItem } from "@mui/material";
import { maxWidth } from "@mui/system";
import { makeRequest } from "../../axios";
import { useQueryClient } from "@tanstack/react-query";
const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    "& .MuiDialogContent-root": {
        padding: theme.spacing(2),
    },
    "& .MuiDialogActions-root": {
        padding: theme.spacing(1),
    },
}));

function BootstrapDialogTitle(props) {
    const { children, onClose, ...other } = props;

    return (
        <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
            {children}
            {onClose ? (
                <IconButton
                    aria-label="close"
                    onClick={onClose}
                    sx={{
                        position: "absolute",
                        right: 8,
                        top: 8,
                        color: (theme) => theme.palette.grey[500],
                    }}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </DialogTitle>
    );
}

export default function UpdatePostModal({ open, setOpen, post }) {
    const queryClient = useQueryClient()
    const [option, setOption] = useState(post.status);
    const [content, setContent] = useState(post.content)
    const [images, setImage] = useState(post.images)
    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div>

            <BootstrapDialog

                fullWidth
                maxWidth="md"
                onClose={handleClose}
                aria-labelledby="customized-dialog-title"
                open={open}
            >
                <BootstrapDialogTitle
                    id="customized-dialog-title"
                    onClose={handleClose}
                >
                    Thay đổi
                </BootstrapDialogTitle>
                <DialogContent dividers></DialogContent>
                <FormControl variant="standard">
                    <TextField
                        sx={{
                            mx: "20px"
                        }}
                        multiline
                        variant="outlined"
                        label="Chia sẻ cảm xúc của bạn"
                        value={content}
                        onChange={(e) => { setContent(e.target.value) }}
                    ></TextField>
                    {/* <Typography
                        sx={
                            {
                                margin: "20px"
                            }
                        }
                        display="inline"


                    >
                        Bạn muốn chia sẻ với
                    </Typography>


                    <Select
                        sx={
                            {
                                margin: "20px",
                                mt: "0px"
                            }
                        }
                        value={option}
                        onChange={(e) => {
                            setOption(e.target.value);
                        }}
                    >
                        <MenuItem value={"public"}>Mọi người</MenuItem>
                        <MenuItem value={"follower"}>Người theo dõi</MenuItem>
                        <MenuItem value={"private"}>Chỉ mình bạn</MenuItem>
                    </Select> */}

                    {/* {
                        images[0]     && (
                            <>
                                <ImageList sx={{ maxHeight: 450, m: 2 }}>
                                    {
                                        images.map((image) => (
                                            <ImageListItem>
                                                <img src={image}></img>
                                            </ImageListItem>
                                        ))
                                    }
                                </ImageList>
                            </>
                        )
                    }
                    <Button onClick={() => { setImage(null) }}>Bỏ ảnh</Button>
                    <Button ><label htmlFor="file">Thay đổi ảnh</label></Button>
                    <input type={"file"} hidden id="file" onInput={(e)=>{}}/> */}
                </FormControl>
                <DialogActions>
                    <Button onClick={handleClose}>
                        Hủy
                    </Button>
                    <Button color="primary" variant="contained" onClick={() => {
                        makeRequest.put("/post/" + post._id, {
                            content, status: option
                        }, {
                            headers: {
                                Authorization: `Bearer ${localStorage.getItem("token")}`
                            }
                        }).then((res) => {
                            queryClient.invalidateQueries(["posts"])
                            setOpen(false)

                            console.log(res)
                        })
                    }}>
                        Thay đổi
                    </Button>
                </DialogActions>
            </BootstrapDialog>
        </div>
    );
}
