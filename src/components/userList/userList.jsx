import { useQuery, useQueryClient } from "@tanstack/react-query";
import { makeRequest } from "../../axios";
import { Table, LinearProgress, Button, Tab } from "@mui/material";
import Box from "@mui/material/Box";
import * as React from "react";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import TableSortLabel from "@mui/material/TableSortLabel";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import Checkbox from "@mui/material/Checkbox";
import IconButton from "@mui/material/IconButton";
import Tooltip from "@mui/material/Tooltip";
import FormControlLabel from "@mui/material/FormControlLabel";
import Switch from "@mui/material/Switch";
import DeleteIcon from "@mui/icons-material/Delete";
import FilterListIcon from "@mui/icons-material/FilterList";
import { visuallyHidden } from "@mui/utils";
import { useNavigate } from "react-router-dom";

function StatusLabel({ user }) {
  const queryClient = useQueryClient();
  return (
    <>
      <Typography display="inline">
        {user.status ? "Đang mở" : "Đang khóa"}
      </Typography>
      <Button
        onClick={(e) => {
          e.stopPropagation();
          makeRequest
            .put(
              `user/${user.status ? "lock" : "unlock"}`,
              {
                id: user._id,
              },
              {
                headers: {
                  Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
              }
            )
            .then(() => {
              queryClient.invalidateQueries(["userList"]);
            });
        }}
      >
        {!user.status ? "Mở" : "Khóa"}
      </Button>
    </>
  );
}
const headCells = [
  {
    id: "fullname",
    numeric: false,
    disablePadding: true,
    label: "Họ và tên",
  },
  {
    id: "email",
    numeric: false,
    disablePadding: false,
    label: "Email",
  },
  {
    id: "followings",
    numeric: true,
    disablePadding: false,
    label: "Số người theo dõi",
    //sort has bug
  },
  {
    id: "followers",
    numeric: true,
    disablePadding: false,
    label: "Số người đang theo dõi",
    //sort has bug
  },
  {
    id: "role",
    numeric: false,
    disablePadding: false,
    label: "Cấp bậc",
  },
  {
    id: "status",
    numeric: false,
    disablePadding: false,
    label: "Trạng thái",
  },
];
function descendingComparator(a, b, orderBy) {
  if (a.constructor === Array && b.constructor === Array) {
    a = a.length;
    b = b.length;
  }
  if (!(typeof a[orderBy] === "string" || a[orderBy] instanceof String)) {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
  } else {
    return b[orderBy].localeCompare(a[orderBy]);
  }
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}
function CustomTableHead({
  order,
  orderBy,

  onRequestSort,
}) {
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        <TableCell>STT</TableCell>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? "right" : "left"}
            padding={headCell.disablePadding ? "none" : "normal"}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <Box component="span" sx={visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}
function EnhancedTable({ rows }) {
 // console.log(rows);
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("fullname");

  const [page, setPage] = React.useState(0);
  // const [dense, setDense] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };
  const navigate = useNavigate();

  // const handleSelectAllClick = (event) => {
  //   if (event.target.checked) {
  //     const newSelected = rows.map((n) => n.name);
  //     setSelected(newSelected);
  //     return;
  //   }
  //   setSelected([]);
  // };

  const handleClick = (event, row) => {
    navigate(`/profile/${row._id}`);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  // const handleChangeDense = (event) => {
  //   setDense(event.target.checked);
  // };

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  const visibleRows = React.useMemo(
    () =>
      rows
        .slice()
        .sort(getComparator(order, orderBy))
        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage),
    [order, orderBy, page, rowsPerPage, rows]
  );
//   console.log("rows ", rows, "visiblerows ", visibleRows);
  return (
    <Box sx={{ width: "100%" }}>
      <Paper sx={{ width: "100%", mb: 2 }}>
        <TableContainer>
          <Table
            sx={{ minWidth: 750 }}
            aria-labelledby="tableTitle"
            //   size={dense ? "small" : "medium"}
          >
            <CustomTableHead
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              //   rowCount={rows.length}
            />
            <TableBody>
              {visibleRows.map((row, index) => {
                const labelId = `enhanced-table-row-${index}`;

                return (
                  <TableRow
                    hover
                    onClick={(event) => handleClick(event, row)}
                    role="checkbox"
                    //   aria-checked={isItemSelected}
                    tabIndex={-1}
                    key={row._id}
                    //   selected={isItemSelected}
                    sx={{ cursor: "pointer" }}
                  >
                    <TableCell>{index + 1 + page * rowsPerPage}</TableCell>
                    <TableCell align="left">{row.fullname}</TableCell>
                    <TableCell align="left">{row.email}</TableCell>
                    <TableCell align="right">{row.followings.length}</TableCell>
                    <TableCell align="right">{row.followers.length}</TableCell>

                    <TableCell align="left">
                      {row.role ? "Người dùng" : "Quản trị viên"}
                    </TableCell>
                    <TableCell align="left">
                      <StatusLabel user={row} />
                    </TableCell>
                  </TableRow>
                );
              })}
              {emptyRows > 0 && (
                <TableRow
                  style={
                    {
                      //   height: (dense ? 33 : 53) * emptyRows,
                    }
                  }
                >
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
      {/* <FormControlLabel
          control={<Switch checked={dense} onChange={handleChangeDense} />}
          label="Dense padding"
        /> */}
    </Box>
  );
}

export default function UserList({ style }) {
  const { isLoading, error, data } = useQuery(["userList"], () =>
    makeRequest.get(
      "user/list-user",
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
     
    )
  );
  // console.log(data.data.user);
  return (
    <div style={style}>
      {isLoading ? (
        <LinearProgress sx={{ textAlign: "center" }} />
      ) : error ? (
        "error"
      ) : (
        <EnhancedTable rows={data.data.user} />
      )}
    </div>
  );
}
